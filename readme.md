## How to try
```sh

  sh rundocker.sh

```
 
 or
   
```sh
    
  docker build -t Flask-simple-restapi:latest .
  docker run -i -t -d -p 5000:5000 flask-simple-restapi:latest

```
## Example getId
```sh
Post  http://localhost:5000/patente/getId
body = {
  "patente":"ZZZZ999"
}
```
> output: 456976000
**
## Example getPatente
```sh
Post  http://localhost:5000/getPatente
body = {
  "id": 456976000
}
```
> output: ZZZZ999
**


made with love :heart: