from flask import Flask, jsonify, request
from string import ascii_uppercase

app = Flask(__name__)

from products import products

# Testing Route
@app.route('/ping', methods=['GET'])
def ping():
    return jsonify({'response': 'pong!'})

# Create Data Routes

# Consulta ID
@app.route('/getId', methods=['POST'])
def getPatenteToId():
    patente = request.json['patente']

    patenteArray = list(patente)
    numbers = patenteArray[4:7]
    letters = patenteArray[0:4]

    if len(letters) != 4 or len(numbers) != 3 or len(patente) != 7:
        return "Error, ingrese una patente valida"
    # validar que recorrido sean letras mayuculas
    for i in range(len(letters)):
        if not letters[i].isupper():
            return "Error, ingrese una patente valida"
    # validar que recorrido sean numeros
    for i in range(len(numbers)):
        if not numbers[i].isdigit():
            return "Error, ingrese una patente valida"


    letterInNumber = convertLetterToNumber(letters)
    id = ( letterInNumber*1000 + int(''.join(numbers)) ) + 1

    print(numbers)
    print(letters)
    print(letterInNumber)
    print(id)
    return jsonify({'id': id})


def convertLetterToNumber(letters):
    out = 0 
    length = len(letters)
    for i in range(length):
        out += (ord(letters[i]) - 65) * (26 ** (length - i - 1))
    return out


# Consulta Patente

@app.route("/getPatente", methods=["POST"])
def idToPatente():
    miId = request.json['id']
    print(miId)
    
    maxId = (26**4)*1000

    if miId is None:
        return jsonify({"error": "ingresa ID valida"})

    if miId == "" or any(char.isalpha() for char in str(miId)):
        return jsonify({"error": "Ingrese una id valida entre 0 y "+str(maxId)})

    miId = int(miId)-1
    patente_letters = ["A","A","A","A"]
    patente_letters_numbers = [0,0,0,0]
    miId_num_int = 0
    miId_num = miId % 1000

    if miId >= 1000:
        miId_num_int = miId // 1000
        
    if miId_num_int // (26**4)% 26 >= 1 or  miId < 0:
        
        return jsonify({"error": "Ingrese una id valida entre 0 y "+str(maxId)})

    if miId_num_int >= 1:
        for i in range(len(patente_letters)):
            patente_letters_numbers[len(patente_letters)-1-i] = miId_num_int // (26**i) % 26
   

    for index, i in enumerate(patente_letters_numbers):
        patente_letters[index] = ascii_uppercase[i]

    patente_letters = "".join(patente_letters)
    patente_numbers = str(miId_num)
    patente_numbers = "0"*(3-len(patente_numbers)) + patente_numbers
    patente = patente_letters + patente_numbers

    return jsonify({"patente": patente})


if __name__ == '__main__':
    app.run(debug=True, port=4000)
